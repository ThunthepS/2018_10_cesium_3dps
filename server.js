/*
 * 3DCityDB-Web-Map
 * http://www.3dcitydb.org/
 * 
 * Copyright 2015 - 2017
 * Chair of Geoinformatics
 * Technical University of Munich, Germany
 * https://www.gis.bgu.tum.de/
 * 
 * The 3DCityDB-Web-Map is jointly developed with the following
 * cooperation partners:
 * 
 * virtualcitySYSTEMS GmbH, Berlin <http://www.virtualcitysystems.de/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *     
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * A simple JavaScript based HTTP server forked from the cesium-starter-app project at https://github.com/pjcozzi/cesium-starter-app.
 **/

(function () {
    "use strict";
    /*global console,require,__dirname,process*/
    /*jshint es3:false*/
    var express = require('express');
    var compression = require('compression');
    var url = require('url');
    var request = require('request');
    var bodyParser = require('body-parser');
    var fs = require('fs');
    var yargs = require('yargs').options({
        'port': {
            'default': 8081,
            'description': 'Port to listen on.'
        },
        'public': {
            'type': 'boolean',
            'description': 'Run a public server that listens on all interfaces.'
        },
        'upstream-proxy': {
            'description': 'A standard proxy server that will be used to retrieve data.  Specify a URL including port, e.g. "http://proxy:8000".'
        },
        'bypass-upstream-proxy-hosts': {
            'description': 'A comma separated list of hosts that will bypass the specified upstream_proxy, e.g. "lanhost1,lanhost2"'
        },
        'help': {
            'alias': 'h',
            'type': 'boolean',
            'description': 'Show this help.'
        }
    });
    var argv = yargs.argv;

    if (argv.help) {
        return yargs.showHelp();
    }

    // eventually this mime type configuration will need to change
    // https://github.com/visionmedia/send/commit/d2cb54658ce65948b0ed6e5fb5de69d022bef941
    var mime = express.static.mime;
    mime.define({
        'application/json': ['czml', 'json', 'geojson', 'topojson'],
        'model/vnd.gltf+json': ['gltf'],
        'model/vnd.gltf.binary': ['bgltf'],
        'text/plain': ['glsl']
    });

    var app = express();
    app.use(compression());
    app.use(express.static(__dirname));
    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });
    app.use(bodyParser.json()); // for parsing application/json
    app.use(bodyParser.urlencoded({
        extended: true
    })); // for parsing application/x-www-form-urlencoded


    function getRemoteUrlFromParam(req) {
        var remoteUrl = req.params[0];
        if (remoteUrl) {
            // add http:// to the URL if no protocol is present
            if (!/^https?:\/\//.test(remoteUrl)) {
                remoteUrl = 'http://' + remoteUrl;
            }
            remoteUrl = url.parse(remoteUrl);
            // copy query string
            remoteUrl.search = url.parse(req.url).search;
        }
        return remoteUrl;
    }

    var dontProxyHeaderRegex = /^(?:Host|Proxy-Connection|Connection|Keep-Alive|Transfer-Encoding|TE|Trailer|Proxy-Authorization|Proxy-Authenticate|Upgrade)$/i;

    function filterHeaders(req, headers) {
        var result = {};
        // filter out headers that are listed in the regex above
        Object.keys(headers).forEach(function (name) {
            if (!dontProxyHeaderRegex.test(name)) {
                result[name] = headers[name];
            }
        });
        return result;
    }

    var upstreamProxy = argv['upstream-proxy'];
    var bypassUpstreamProxyHosts = {};
    if (argv['bypass-upstream-proxy-hosts']) {
        argv['bypass-upstream-proxy-hosts'].split(',').forEach(function (host) {
            bypassUpstreamProxyHosts[host.toLowerCase()] = true;
        });
    }

    app.get('/proxy/*', function (req, res, next) {
        // look for request like http://localhost:8080/proxy/http://example.com/file?query=1
        var remoteUrl = getRemoteUrlFromParam(req);
        if (!remoteUrl) {
            // look for request like http://localhost:8080/proxy/?http%3A%2F%2Fexample.com%2Ffile%3Fquery%3D1
            remoteUrl = Object.keys(req.query)[0];
            if (remoteUrl) {
                remoteUrl = url.parse(remoteUrl);
            }
        }

        if (!remoteUrl) {
            return res.status(400).send('No url specified.');
        }

        if (!remoteUrl.protocol) {
            remoteUrl.protocol = 'http:';
        }

        var proxy;
        if (upstreamProxy && !(remoteUrl.host in bypassUpstreamProxyHosts)) {
            proxy = upstreamProxy;
        }

        // encoding : null means "body" passed to the callback will be raw bytes

        request.get({
            url: url.format(remoteUrl),
            headers: filterHeaders(req, req.headers),
            encoding: null,
            proxy: proxy
        }, function (error, response, body) {
            var code = 500;

            if (response) {
                code = response.statusCode;
                res.header(filterHeaders(req, response.headers));
            }

            res.status(code).send(body);
        });
    });

    // Joe part
    // Add the connection to the 3DPS Service
    app.post('/load_collection', function (req, res) {
        try {
            console.log("Post from Client URL: " + req.body.url);

            request.get(`${req.body.url}/collections`, function (error, response, body) {
                try {
                    var jsonbody = JSON.parse(body);
                    var collection_id = `<div class="minibackdrop">`
                    collection_id += `<small class="form-text text-muted">Container: <a href="${req.body.url}/collections/" target="_blank">${req.body.url}/collections/</a></small>`


                    for (let index = 0; index < jsonbody.collections.length; index++) {
                        const element = jsonbody.collections[index];
                        var TileSetURL = ``
                        var i3sURL = ``
                        var urlText = ``
                        urlText += `<br><b>${element.id}</b>`
                        // For looping in the collections/link
                        // looping to get the content*** Skymantics + 3D Hypothetical Horse are working!~

                        for (let index = 0; index < element.links.length; index++) {
                            var link = element.links[index];
                            if (link.type == "application/json+3dtiles") {
                                TileSetURL += link.href
                                urlText += `<br><button class="buttonBlue colorLightBlue" id="${element.id}" onclick="loadTileset('${TileSetURL}','')">Load 3D Tiles</button>`
                            } else if (link.type == "application/json+i3s") {
                                i3sURL += link.href
                                urlText += `<br><button class="buttonBlue colorLightPurple" id="${element.id}" onclick="load_i3s('${i3sURL}')">Load i3s</button>`
                            }
                        }
                        // Create 3D Tile and i3s link seperately for STT because of lacking 3dTile link in the collections

                        // if (req.body.url.includes("steinbeis-3dps")) {
                        //     TileSetURL = 'http://steinbeis-3dps.eu:8080/3DContainerTile/collections/NewYork/3DTiles'
                        //     i3sURL = 'https://tiles.arcgis.com/tiles/P3ePLMYs2RVChkJx/arcgis/rest/services/Buildings_NewYork_17/SceneServer/layers/0'
                        //     urlText += `<br><button class="buttonBlue colorLightBlue" id="${element.id}" onclick="loadTileset('${TileSetURL}','')">Load 3D Tiles</button>`
                        //     urlText += `<br><button class="buttonBlue colorLightPurple" id="${element.id}" onclick="load_i3s('${i3sURL}')">Load i3s</button>`
                        // }

                        collection_id += urlText
                    }
                    collection_id += `</div>`;
                    res.send(collection_id);
                } catch (error) {
                    res.send("error");
                    console.log(error)
                }

            })
        } catch (error) {
            res.send("error");
        }

    })
    app.post('/i3s_page_request', function (req, res) {
        res.set('Content-Type', 'text/html');
        res.send(new Buffer('<h2>Test String</h2>'));
    })

    app.post('/3dpsRequest', function (req, res) {

        console.log("Post from Client URL: " + req.body.url);

        request.get(req.body.url, function (error, response, body) {
            // console.log("Body: " + body)
            try {
                var tilesetURL = [];
                var body1 = JSON.parse(body);
                var body2 = body1.tilesetLink;
                tilesetURL.push(body2);
                var tilesetString = JSON.stringify(body2);
                var mainurl = tilesetString.replace('tileset.json', 'b3dms');
                mainurl = mainurl.replace(/\"/g, "")
                console.log("Response: " + tilesetURL);
                console.log("MainUrl: " + mainurl);

                request.get(body2, function (error, response, bodyString) {
                    var TilesetEdit = [];

                    var bodyString2 = bodyString.replace(/\/b3dms/g, mainurl);
                    bodyString2 = bodyString2.replace(/url/g, "uri");
                    bodyString2 = bodyString2.replace(/add/g, "ADD");
                    TilesetEdit.push(JSON.parse(bodyString2));
                    fs.writeFile("3DPS_App/Data/tileset_temp.json", JSON.stringify(TilesetEdit[0]), function (err) {
                        if (err) throw err;
                        console.log('file saved');
                        res.send(tilesetURL);

                    });
                });
            } catch (error) {
                console.log("Request to http://steinbeis-3dps.eu:8080 is error :" + error)
                res.send("no-response");
            }
            // if (error) {
            //     console.log("request error:" + error);
            // }
        });

    })
    app.get('/tileset.json', function (req, res) {
        // res.send(TilesetEdit)
        res.send(TilesetEdit[0])

    })





    // -------------------------------------------------------------
    var server = app.listen(argv.port, '0.0.0.0', function () {
        console.log('3D data container application (3D Tiles) running at http://localhost:%d/3DPS_App/', server.address().port);
        console.log('3D data container application (i3s) running at http://localhost:%d/3DPS_App/geoportal-i3s.html', server.address().port);
    });

    server.on('error', function (e) {
        if (e.code === 'EADDRINUSE') {
            console.log('Error: Port %d is already in use, select a different port.', argv.port);
            console.log('Example: node server.js --port %d', argv.port + 1);
        } else if (e.code === 'EACCES') {
            console.log('Error: This process does not have permission to listen on port %d.', argv.port);
            if (argv.port < 1024) {
                console.log('Try a port number higher than 1024.');
            }
        }
        console.log(e);
        process.exit(1);
    });

    server.on('close', function () {
        console.log('Cesium development server stopped.');
    });

    var isFirstSig = true;
    process.on('SIGINT', function () {
        if (isFirstSig) {
            console.log('Cesium development server shutting down.');
            server.close(function () {
                process.exit(0);
            });
            isFirstSig = false;
        } else {
            console.log('Cesium development server force kill.');
            process.exit(1);
        }
    });

})();