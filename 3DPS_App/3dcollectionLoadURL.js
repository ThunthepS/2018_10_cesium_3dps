$("#submitCollectionURL").click(function () {
  // alert( "Loading collection: " + $("#collectionURL").val() );
  postCollection($("#collectionURL").val())
});

function postCollection(requestURI) {
  // check if the URL contains "/" in the last character or not 
  if (requestURI.charAt(requestURI.length - 1) == "/") {
    requestURI = requestURI.slice(0, -1);
  }
  $("#loadingURL").show()
  // $.getJSON( `${requestURI}/collections`, function( data ) {
  //   var collection_id = "";
  //   for (let index = 0; index < data.collections.length; index++) {
  //     const element = array[index];
  //     collection_id += element.id;
  //   }
  //   $("#collectionResult").html(collection_id)
  // })




  $.post("/load_collection",
    {
      url: requestURI,
    },
    function (data, status) {
      $("#loadingURL").hide()

      // alert("Data: " + data + "\nStatus: " + status);
      $("#collectionResult").html(data)

    });
}