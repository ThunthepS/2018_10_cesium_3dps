Cesium.BingMapsApi.defaultKey = 'ApOW9LMkerqWIVSnFauilSeaZyp8df66byy1USCTjgTdMvhb4y1iAhEsUHQfCgzq'
Cesium.Ion.defaultAccessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJmOGQ1ODcwNC0zNDU1LTQ5ZGEtOWE4YS0zYzNmYTcxOWQxYTUiLCJpZCI6MTAxMywiaWF0IjoxNTI2NjM1Nzg4fQ.X43-3OHqmcwPP5qCE2NqSGJUsj5FGWgXAzitvrV0pwY';
var host = "local"; // set local or Coors
var urlprefix;
if (host === "Coors") {
    urlprefix = "http://81.169.187.7:8080/ebikesharing";
} else if (host === "local") {
    urlprefix = ".";
}

var collectionLink = "3dtiles"
var viewer = new Cesium.Viewer('cesiumContainer', {
    // terrainProvider: Cesium.createWorldTerrain(),
    // imageryProvider: Cesium.createOpenStreetMapImageryProvider({
    //     url: 'https://a.tile.openstreetmap.org/'
    // }), //use 2d instead of 3d
    baseLayerPicker: true,
    homeButton: false,
    // scene3DOnly: true,
    //terrainExaggeration: 1.5,
    baseLayerPicker : false,
    navigationHelpButton: false,
    selectionIndicator: false,
    sceneModePicker: false,
    geocoder: false,
    // shadows: false,
    imageryProvider : new Cesium.IonImageryProvider({ assetId: 4 })
});
// Basic Set Up for the Cesium Globe
viewer.scene.globe.enableLighting = true;



// Basic Set Up for view
//initial camera 
var camRect = Cesium.Rectangle.fromDegrees(-74.2554824, 40.498434755378184, -73.700049137171745883, 40.915055674909346806);
viewer.camera.setView({
    destination: camRect
});

// A Function to turn off terrain
var DestroyView = function () {
    viewer.destroy();
};

var removeLayers = function () {
    webMap.removeLayer(dataLayer.id);
    viewer.trackedEntity = undefined;
    viewer.scene.camera.flyTo(homeCameraView);
};
var initialPosition = new Cesium.Cartesian3.fromDegrees((-74.2554824 - 73.700049137171745883) / 2, (40.498434755378184 + 40.915055674909346806) / 2, 1631.082799425431);
var initialOrientation = new Cesium.HeadingPitchRoll.fromDegrees(7.1077496389876024807, -31.987223091598949054, 0.025883251314954971306);
var homeCameraView = {
    destination: initialPosition,
    orientation: {
        heading: initialOrientation.heading,
        pitch: initialOrientation.pitch,
        roll: initialOrientation.roll
    }
};
var flyNY = function () {
    //NY Area
    viewer.camera.flyTo({
        destination: {
            x: 1337773.6995213495,
            y: -4663539.045573576,
            z: 4138974.4747825917
        },
        orientation: {
            direction: {
                x: -0.27496797577614784,
                y: 0.9585506814592859,
                z: 0.07465389053161531
            },
            up: {
                x: 0.020584871145735848,
                y: -0.0717597829666441,
                z: 0.9972095048827471
            }
        }
    });
}